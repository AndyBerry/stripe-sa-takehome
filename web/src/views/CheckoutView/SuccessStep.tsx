import { Link as RRLink } from "react-router-dom";

import {
  Alert,
  AlertTitle,
  Card,
  CardContent,
  Link,
  Stack,
  Typography,
} from "@mui/material";

import { currencyFormatter } from "../../utils/currentyFormatter";
import { useRetrievePaymentIntentQuery } from "./useRetrievePaymentIntentQuery";

interface SuccessStepProps {
  paymentIntentClientSecret: string;
}

export function SuccessStep({ paymentIntentClientSecret }: SuccessStepProps) {
  const retrievePaymentIntentQuery = useRetrievePaymentIntentQuery(
    paymentIntentClientSecret
  );

  const paymentIntent = retrievePaymentIntentQuery.data?.paymentIntent;
  if (!paymentIntent) {
    return null;
  }

  return (
    <Card>
      <CardContent>
        <Stack direction="column" spacing={2}>
          <Alert variant="filled" severity="success">
            <AlertTitle>Payment success!</AlertTitle>
            Your provided payment method has been charged{" "}
            <strong>
              {currencyFormatter.format(paymentIntent.amount / 100)}
            </strong>
            .
          </Alert>

          <Typography
            variant="body2"
            sx={{ textAlign: "center", color: "text.secondary" }}
          >
            Payment ID: <strong>{paymentIntent.id}</strong>
          </Typography>
        </Stack>
      </CardContent>

      <CardContent sx={{ background: "#f9f9f9" }}>
        <Link component={RRLink} to="/">
          Return to shop
        </Link>
      </CardContent>
    </Card>
  );
}
