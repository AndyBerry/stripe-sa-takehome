import { Box, Card, CardContent, CircularProgress } from "@mui/material";

import { PaymentDetailsStep } from "./PaymentDetailsStep";
import { SuccessStep } from "./SuccessStep";

import { useRetrievePaymentIntentQuery } from "./useRetrievePaymentIntentQuery";

interface CheckoutWizardProps {
  paymentIntentClientSecret: string;
}

export function CheckoutWizard({
  paymentIntentClientSecret,
}: CheckoutWizardProps) {
  const retrievePaymentIntentQuery = useRetrievePaymentIntentQuery(
    paymentIntentClientSecret
  );

  const paymentIntentStatus =
    retrievePaymentIntentQuery.data?.paymentIntent?.status;

  if (paymentIntentStatus === "succeeded") {
    return (
      <SuccessStep paymentIntentClientSecret={paymentIntentClientSecret} />
    );
  }

  if (paymentIntentStatus === "requires_payment_method") {
    return (
      <PaymentDetailsStep
        paymentIntentClientSecret={paymentIntentClientSecret}
      />
    );
  }

  return (
    <Card>
      <CardContent>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            minHeight: 200,
          }}
        >
          <CircularProgress />
        </Box>
      </CardContent>
    </Card>
  );
}
