import { useElements, useStripe } from "@stripe/react-stripe-js";
import { useMutation, useQueryClient } from "react-query";

export function useConfirmPaymentMutation() {
  const queryClient = useQueryClient();
  const stripe = useStripe();
  const elements = useElements();

  return useMutation(
    async (paymentIntentClientSecret: string) => {
      if (!stripe || !elements) {
        throw new Error(
          "Payment integration not ready yet - please try again shortly and contact us if this issue persists."
        );
      }

      const { error } = await stripe.confirmPayment({
        elements,
        confirmParams: {
          return_url: process.env.REACT_APP_STRIPE_RETURN_URL,
        },
        redirect: "if_required",
      });

      if (error) {
        throw new Error(error.message);
      }
    },
    {
      onSettled(data, error, paymentIntentClientSecret) {
        queryClient.invalidateQueries([
          "paymentIntent",
          paymentIntentClientSecret,
        ]);
      },
    }
  );
}
