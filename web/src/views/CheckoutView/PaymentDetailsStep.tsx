import { useState } from "react";
import { Link as RRLink } from "react-router-dom";
import { PaymentElement } from "@stripe/react-stripe-js";

import {
  Alert,
  Box,
  Card,
  CardContent,
  Link,
  Stack,
  Typography,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";

import { currencyFormatter } from "../../utils/currentyFormatter";
import { useConfirmPaymentMutation } from "./useConfirmPaymentMutation";
import { useRetrievePaymentIntentQuery } from "./useRetrievePaymentIntentQuery";

interface PaymentDetailsStepProps {
  paymentIntentClientSecret: string;
}

export function PaymentDetailsStep({
  paymentIntentClientSecret,
}: PaymentDetailsStepProps) {
  const confirmPaymentMutation = useConfirmPaymentMutation();
  const retrievePaymentIntentQuery = useRetrievePaymentIntentQuery(
    paymentIntentClientSecret
  );

  const [hasSuccessfullyPaid, setHasSuccessfullyPaid] = useState(false);
  const [isReady, setIsReady] = useState(false);

  const lastError =
    retrievePaymentIntentQuery.data?.paymentIntent?.last_payment_error;

  const amount = retrievePaymentIntentQuery.data?.paymentIntent?.amount;

  return (
    <form
      id="payment-form"
      onSubmit={(e) => {
        e.preventDefault();
        confirmPaymentMutation.mutate(paymentIntentClientSecret, {
          onSuccess: () => setHasSuccessfullyPaid(true),
        });
      }}
    >
      <Card>
        <CardContent>
          <Stack direction="column" spacing={2}>
            <Typography>Please enter your payment details below:</Typography>

            {!!lastError ? (
              <Alert variant="filled" severity="error">
                {lastError.message}
              </Alert>
            ) : null}

            <Box sx={{ minHeight: 120 }}>
              <PaymentElement onReady={() => setIsReady(true)} />
            </Box>
          </Stack>
        </CardContent>

        <CardContent sx={{ background: "#f9f9f9" }}>
          <Stack
            direction="row-reverse"
            sx={{ justifyContent: "space-between", alignItems: "center" }}
          >
            <LoadingButton
              disabled={!isReady}
              loading={confirmPaymentMutation.isLoading || hasSuccessfullyPaid}
              variant="contained"
              type="submit"
            >
              {amount ? `Pay ${currencyFormatter.format(amount / 100)}` : "Pay"}
            </LoadingButton>

            {!confirmPaymentMutation.isLoading && !hasSuccessfullyPaid ? (
              <Link component={RRLink} to="/">
                Return to shop
              </Link>
            ) : null}
          </Stack>
        </CardContent>
      </Card>
    </form>
  );
}
