import { loadStripe } from "@stripe/stripe-js";
import { Navigate, useSearchParams } from "react-router-dom";
import { Elements } from "@stripe/react-stripe-js";

import { Grid } from "@mui/material";

import { CheckoutSessionProductCard } from "./CheckoutSessionProductCard";
import { CheckoutWizard } from "./CheckoutWizard";

if (!process.env.REACT_APP_STRIPE_PUBLIC_KEY) {
  throw new Error("Missing env variable: REACT_APP_STRIPE_PUBLIC_KEY");
}
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLIC_KEY);

export function CheckoutViewWrapper() {
  const [searchParams] = useSearchParams();
  const paymentIntentClientSecret = searchParams.get(
    "payment_intent_client_secret"
  );

  return paymentIntentClientSecret ? (
    <CheckoutView paymentIntentClientSecret={paymentIntentClientSecret} />
  ) : (
    <Navigate to="/" />
  );
}

interface CheckoutViewProps {
  paymentIntentClientSecret: string;
}

function CheckoutView({ paymentIntentClientSecret }: CheckoutViewProps) {
  return (
    <Elements
      options={{
        clientSecret: paymentIntentClientSecret,
        appearance: {
          theme: "flat",
        },
      }}
      stripe={stripePromise}
    >
      <Grid container spacing={4}>
        <Grid item xs={12} md={5}>
          <CheckoutSessionProductCard
            paymentIntentClientSecret={paymentIntentClientSecret}
          />
        </Grid>

        <Grid item xs={12} md={7}>
          <CheckoutWizard
            paymentIntentClientSecret={paymentIntentClientSecret}
          />
        </Grid>
      </Grid>
    </Elements>
  );
}
