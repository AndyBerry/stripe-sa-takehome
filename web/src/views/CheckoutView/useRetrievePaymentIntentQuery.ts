import { useStripe } from "@stripe/react-stripe-js";
import { useQuery } from "react-query";

export function useRetrievePaymentIntentQuery(
  paymentIntentClientSecret: string
) {
  const stripe = useStripe();

  return useQuery(
    ["paymentIntent", paymentIntentClientSecret],
    async () => {
      if (!stripe) {
        throw new Error();
      }

      return stripe.retrievePaymentIntent(paymentIntentClientSecret);
    },
    {
      enabled: !!stripe,
    }
  );
}
