import { ProductCard } from "../../components/ProductCard/ProductCard";
import { ProductCardError } from "../../components/ProductCard/ProductCardError";
import { ProductCardSkeleton } from "../../components/ProductCard/ProductCardSkeleton";

import { useRetrievePaymentIntentQuery } from "./useRetrievePaymentIntentQuery";

interface CheckoutSessionProductCardProps {
  paymentIntentClientSecret: string;
}

export function CheckoutSessionProductCard({
  paymentIntentClientSecret,
}: CheckoutSessionProductCardProps) {
  const retrievePaymentIntentQuery = useRetrievePaymentIntentQuery(
    paymentIntentClientSecret
  );

  if (retrievePaymentIntentQuery.isError) {
    return <ProductCardError />;
  }

  const productId = retrievePaymentIntentQuery.data?.paymentIntent?.description;
  if (!productId) {
    return <ProductCardSkeleton />;
  }

  return <ProductCard productId={productId} />;
}
