import { useNavigate } from "react-router-dom";
import { useMutation } from "react-query";
import { useSnackbar } from "notistack";

async function createCheckoutSession(productId: string): Promise<{
  paymentIntentClientSecret: string;
}> {
  const response = await fetch(
    `${process.env.REACT_APP_API_BASE}/checkoutsessions`,
    {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ productId }),
    }
  );

  if (!response.ok) {
    throw new Error();
  }

  return await response.json();
}

export function useCreateCheckoutSessionMutation() {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  return useMutation(createCheckoutSession, {
    onSuccess(data) {
      navigate(
        `/checkout?payment_intent_client_secret=${data.paymentIntentClientSecret}`
      );
    },
    onError() {
      enqueueSnackbar(
        "There was an error processing your checkout session. Please try again and contact us if the error persists.",
        { variant: "error" }
      );
    },
  });
}
