import { useIsMutating } from "react-query";

import { LoadingButton } from "@mui/lab";

import { ProductCard } from "../../components/ProductCard/ProductCard";

import { useCreateCheckoutSessionMutation } from "../../views/ProductListView/useCreateCheckoutSessionMutation";

interface BuyableProductCardProps {
  productId: string;
}

export function BuyableProductCard({ productId }: BuyableProductCardProps) {
  const isMutating = useIsMutating() > 0;
  const createCheckoutSessionMutation = useCreateCheckoutSessionMutation();

  return (
    <ProductCard productId={productId}>
      <LoadingButton
        variant="contained"
        size="large"
        loading={createCheckoutSessionMutation.isLoading}
        disabled={isMutating}
        onClick={() => createCheckoutSessionMutation.mutate(productId)}
      >
        Buy
      </LoadingButton>
    </ProductCard>
  );
}
