import { useQuery } from "react-query";

async function fetchProductList(): Promise<ReadonlyArray<string>> {
  const response = await fetch(`${process.env.REACT_APP_API_BASE}/products`);

  if (!response.ok) {
    throw new Error();
  }

  return await response.json();
}

export function useProductListQuery() {
  return useQuery(["products"], fetchProductList);
}
