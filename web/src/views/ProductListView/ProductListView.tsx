import { Alert, AlertTitle, Grid } from "@mui/material";

import { BuyableProductCard } from "./BuyableProductCard";

import { useProductListQuery } from "./useProductListQuery";

export function ProductListView() {
  const productList = useProductListQuery();

  if (productList.isError) {
    return (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>

        An error has occurred while attempting to fetch our list of products.
        Please try again and contact us if the issue persists.
      </Alert>
    );
  }

  return productList.data ? (
    <Grid container spacing={4}>
      {productList.data.map((productId) => (
        <Grid key={productId} item xs={12} sm={6} md={4}>
          <BuyableProductCard productId={productId} />
        </Grid>
      ))}
    </Grid>
  ) : null;
}
