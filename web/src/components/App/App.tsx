import { Route, Routes } from "react-router-dom";

import { Layout } from "./Layout";
import { ProductListView } from "../../views/ProductListView/ProductListView";
import { CheckoutViewWrapper } from "../../views/CheckoutView/CheckoutView";

export function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<ProductListView />} />
        <Route path="/checkout" element={<CheckoutViewWrapper />} />
      </Route>
    </Routes>
  );
}
