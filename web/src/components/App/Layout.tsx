import { Box, Container, LinearProgress, Typography } from "@mui/material";
import { useIsFetching } from "react-query";
import { Outlet } from "react-router-dom";

export function Layout() {
  const isFetching = useIsFetching();

  return (
    <Box
      sx={{
        position: "relative",
        display: "flex",
        flexDirection: "column",
        pt: 0.5,
        minHeight: "100vh",
        background: "#fefefe",
      }}
    >
      {isFetching ? (
        <Box sx={{ position: "absolute", top: 0, left: 0, width: "100%" }}>
          <LinearProgress />
        </Box>
      ) : null}

      <Box sx={{ py: 4 }}>
        <Container fixed maxWidth="md">
          <Typography variant="h1">Bookstore</Typography>
        </Container>
      </Box>

      <Box sx={{ flex: "1 0 0" }}>
        <Container fixed maxWidth="md">
          <Outlet />
        </Container>
      </Box>

      <Box sx={{ py: 4 }}>
        <Container fixed maxWidth="md">
          <Typography variant="body2">Built by Andy</Typography>
        </Container>
      </Box>
    </Box>
  );
}
