import { ReactNode } from "react";

import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";

import { ProductCardError } from "./ProductCardError";
import { ProductCardSkeleton } from "./ProductCardSkeleton";

import { useGetProductQuery } from "./useGetProductQuery";
import { currencyFormatter } from "../../utils/currentyFormatter";

interface ProductCardProps {
  productId: string;
  children?: ReactNode;
}

export function ProductCard({ productId, children }: ProductCardProps) {
  const getProductQuery = useGetProductQuery(productId);

  if (getProductQuery.isError) {
    return <ProductCardError />;
  }

  if (!getProductQuery.data) {
    return <ProductCardSkeleton />;
  }

  const { image, author, title, description, price } = getProductQuery.data;
  const formattedPrice = currencyFormatter.format(price / 100);

  return (
    <Card>
      <Box sx={{ position: "relative" }}>
        <CardMedia
          component="img"
          src={image}
          sx={{ width: "100%", aspectRatio: "1280 / 911", background: "#ccc" }}
        />

        <Box
          sx={{
            position: "absolute",
            right: 10,
            bottom: 10,
            px: 1,
            py: 0.5,
            borderRadius: 1,
            background: "#fff",
          }}
        >
          {formattedPrice}
        </Box>
      </Box>

      <CardContent>
        <Typography variant="body2" gutterBottom>
          {author}
        </Typography>
        <Typography variant="h5" gutterBottom>
          {title}
        </Typography>
        <Typography variant="body1" color="text.secondary">
          {description}
        </Typography>
      </CardContent>

      {children ? (
        <CardActions sx={{ justifyContent: "flex-end", background: "#f9f9f9" }}>
          {children}
        </CardActions>
      ) : null}
    </Card>
  );
}
