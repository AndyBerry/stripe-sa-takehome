import { useQuery } from "react-query";

interface ProductDetails {
  id: string;
  title: string;
  author: string;
  description: string;
  image: string;
  price: number;
}

async function getProductById(
  productId: string
): Promise<Readonly<ProductDetails>> {
  const response = await fetch(
    `${process.env.REACT_APP_API_BASE}/products/${productId}`
  );

  if (!response.ok) {
    throw new Error();
  }

  return await response.json();
}

export function useGetProductQuery(productId: string) {
  return useQuery(["products", productId], () => getProductById(productId));
}
