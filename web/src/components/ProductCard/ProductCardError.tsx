import { Alert, AlertTitle } from "@mui/material";

export function ProductCardError() {
  return (
    <Alert severity="error">
      <AlertTitle>Unable to display product</AlertTitle>
      An error has occurred while attempting to fetch this product's
      information. Please try again and contact us if the issue persists.
    </Alert>
  );
}
