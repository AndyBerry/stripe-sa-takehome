import { Card, CardContent, Skeleton, Typography } from "@mui/material";

export function ProductCardSkeleton() {
  return (
    <Card>
      <Skeleton
        variant="rectangular"
        sx={{ aspectRatio: "1280 / 911", height: "auto", width: "100%" }}
      />

      <CardContent>
        <Typography variant="body2" gutterBottom>
          <Skeleton variant="text" sx={{ width: 120 }} />
        </Typography>
        <Typography variant="h5">
          <Skeleton variant="text" />
        </Typography>
      </CardContent>
    </Card>
  );
}
