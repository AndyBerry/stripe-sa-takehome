# Bookstore e-commerce built with Stripe

This is a simple e-commerce application that customers can use to purchase a book.

This application architecture was designed around the Stripe [Payment Intents API](https://stripe.com/docs/payments/payment-intents) to support a custom (and potentially multi-step) payment flow. The payment screen uses Stripe Elements' [Payment Element](https://stripe.com/docs/payments/payment-element) to securely accept all necessary payment details from use in confirming a payment intent.

## Table of contents

- [1. Running the application](#1-running-the-application)
    - [1.1. On initial startup](#11-on-initial-startup)
    - [1.2. Starting the API server and web client](#12-starting-the-api-server-and-web-client)
- [2. Application overview](#2-application-overview)
    - [2.1. The web client](#21-the-web-client)
      - [2.1.1. The product list screen](#211-the-product-list-screen)
      - [2.1.2. The checkout screen](#212-the-checkout-screen)
    - [2.2. The API server](#22-the-api-server)
- [3. Future improvements](#3-future-improvements)

## 1. Running the application

### 1.1. On initial startup

Install dependencies for both the `web` and `api` modules by executing the below commands from the application's root directory:

```bash
npm install --prefix api
npm install --prefix web
```

Next, you will need to create an `.env` file in each of the `/api` and `/web` directories. Note: you can copy the existing sample files by executing the below command from the application's root directory:

```bash
cp web/.env.sample web/.env && cp api/.env.sample api/.env
```

Required environment variables by module:

API server (`/api/.env`)

|Variable name|Description|Example value|
|---|---|---|
|`SERVER_PORT`|Network port that the server will be listening to|`3030`|
|`CORS_ALLOW_ORIGINS`|CORS origin for the web client|`http://localhost:3000`|
|`STRIPE_API_KEY`|Stripe API secret key|`sk_test_XXXXXXXXXXXXXXXX`|

Web client (`/web/.env`)

|Variable name|Description|Example value|
|---|---|---|
|`REACT_APP_API_BASE`|API server base URL|`http://localhost:3030`|
|`REACT_APP_STRIPE_PUBLIC_KEY`|Stripe API publishable key|`pk_test_XXXXXXXXXXXXXXXX`|
|`REACT_APP_STRIPE_RETURN_URL`|Checkout page URL used by Stripe if a redirect is required during payment intent confirmation|`http://localhost:3000/checkout`|

### 1.2. Starting the API server and web client

Execute the below command from the project's root directory:

```bash
npm run start --prefix api
```

In a separate terminal window, execute the below command from the project's root directory:

```bash
npm run start --prefix web
```

## 2. Application overview

This monorepo contains two modules, the API server (`/api`) and the web client (`/web`).

### 2.1. The web client

The web client module was bootstrapped with `create-react-app` using the Typescript template. Third party libraries used include `react-router-dom`, `react-query`, `notistack` and Material UI (`mui`). The custom payment workflow uses the `@stripe/react-stripe-js` and `@stripe/stripe-js` libraries.

The purchase user journey is supported by two screens:

#### 2.1.1. The product list screen

**Route pattern:** `/`

On mount, this screen fetches a list of product IDs from the `GET /products` API endpoint. For each product ID retrieved, a `<ProductCard />` component is rendered (passing the product ID as a prop) with the initial render displaying a loading skeleton. The `<ProductCard />` components will then fetch the appropriate product details from the `GET /products/:productId` API endpoint, switching to the non-loading variant once the request has resolved.

Clicking the "BUY" button will asynchronously create a Stripe payment intent by calling the `POST /checkoutsessions` API endpoint, passing the selected product ID in the request body. If successful, the client will then be redirected to the `/checkout?payment_intent_client_secret=XXXX` screen to complete the payment flow.

![A sequence diagram describing the product list screen UX](docs/product-list-screen.drawio.svg)

#### 2.1.2. The checkout screen

**Route pattern:** `/checkout?payment_intent_client_secret=XXXX`

This screen is used to complete a payment intent provided in the URL search parameter as a payment intent client secret.

On mount, this screen will fetch the publishable details of the payment intent by querying the Stripe API directly. Depending on the `status` of the payment intent object, one of two sub-screens will be displayed. Both sub-screens are wrapped in an `<Elements />` provider component with the current payment intent client secret.

This screen will also display the product selected in the previous step. The product ID used to generate the payment intent is currently being stored under the payment intent's `description` field as this is a publishable field which can be queried with the payment intent client secret alone. Better options for this mapping of product to payment intent can be found in section [3. Future improvements](#3-future-improvements) below.

- A `status` of `succeeded` will display the confirmation message, `amount` and `id` values.
- A `status` of `requires_payment_method` will display the Stripe Elements `<PaymentElement />` component and any applicable messages listed under the payment intent's `last_payment_error` property.

When the form containing the `<PaymentElement />` component is submitted, confirmation of the payment intent is then attempted via the [`Stripe.confirmPayment()` method](https://stripe.com/docs/js/payment_intents/confirm_payment) and once settled, the publishable details of the payment intent are then re-fetched, rendering one of these two sub-screens once again.

![A sequence diagram describing the checkout screen UX](docs/checkout-screen.drawio.svg)

### 2.2. The API server

The API server currently serves two RESTful resources, see the table below for supported endpoints.

|Request|Description|Example return value|
|---|---|---|
|`GET /products`|List all product IDs as an array of strings.|`["1","2","3"]`|
|`GET /products/:productId`|Get a single product's details. Expects a product ID to be provided in the request URI.|`{"id":"...","title":"...",...}`|
|`POST /checkoutsessions`|Using the [`Stripe.PaymentIntentsResource.create()` method](https://stripe.com/docs/api/payment_intents/create?lang=node), creates a Stripe payment intent based on the product ID provided in the request body (as `productId`). Returns the newly created payment intent's client secret. As mentioned above, the product ID associated with this payment intent is currently being stored in the payment intent's `description` field so it can be retrieved later with only the payment intent client secret.|`{"paymentIntentClientSecret":"..."}`|

## 3. Future improvements

|Name|Description|
|---|---|
|Implement a proper inventory management system|The current `productsResource` abstraction has been constructed in a way that can easily be replaced in the future - descriptive names and async functions.|
|Shopping cart|Giving users the ability to purchase more than one item at a time. Cart contents could be persisted client side or server side (attached to a user profile, for example) to improve the cart abandonment metric. Server side persistence could also remove the current need to store the selected product ID in the payment intent's `description` field.|
|Product list search and filter controls|Related to the inventory management system mentioned above. The `api` module's productsResource interface could be expanded to include pagination, filter and search parameters.|
|Native app clients|The `web` module is a React app which consumes the `api` module's REST API and is intended for viewing in a browser. With the current application architecture, additional client modules could be created as native iOS or Android apps that consume the existing API.|