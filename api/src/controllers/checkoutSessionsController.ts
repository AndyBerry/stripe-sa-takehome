import Koa from "koa";
import Stripe from "stripe";

import * as productsResource from "../resources/productsResource";

const stripe = new Stripe(process.env.STRIPE_API_KEY as string, {
  apiVersion: "2020-08-27",
});

export async function handleCreateCheckoutSession(
  ctx: Koa.Context,
  next: CallableFunction
) {
  const productId: string = ctx.request.body.productId;
  const product = await productsResource.getProductById(productId);

  const paymentIntent = await stripe.paymentIntents.create({
    description: productId,
    amount: product.price,
    currency: "aud",
  });

  if (!paymentIntent.client_secret) {
    ctx.throw(500);
  }

  ctx.body = { paymentIntentClientSecret: paymentIntent.client_secret };
  await next();
}
