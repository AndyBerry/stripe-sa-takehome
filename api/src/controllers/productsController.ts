import Koa from "koa";

import * as productsResource from "../resources/productsResource";

export async function handleListProducts(
  ctx: Koa.Context,
  next: CallableFunction
) {
  ctx.body = await productsResource.listProductIds();

  await next();
}

export async function handleGetProductById(
  ctx: Koa.Context,
  next: CallableFunction
) {
  const { productId } = ctx.request.params;

  try {
    const product = await productsResource.getProductById(productId);
    ctx.body = product;
  } catch (err) {
    ctx.throw(404);
  }

  await next();
}
