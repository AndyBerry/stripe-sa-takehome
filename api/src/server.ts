import dotenv from "dotenv";
dotenv.config();

import Koa from "koa";
import KoaJoiRouter from "koa-joi-router";
import KoaCors from "@koa/cors";

import * as productsController from "./controllers/productsController";
import * as checkoutSessionsController from "./controllers/checkoutSessionsController";

const app = new Koa();
const router = KoaJoiRouter();

router.route([
  {
    method: "get",
    path: "/products",
    handler: productsController.handleListProducts,
  },

  {
    method: "get",
    path: "/products/:productId",
    validate: {
      params: { productId: KoaJoiRouter.Joi.string().required() },
    },
    handler: productsController.handleGetProductById,
  },

  {
    method: "post",
    path: "/checkoutsessions",
    validate: {
      body: { productId: KoaJoiRouter.Joi.string().required() },
      type: "json",
    },
    handler: checkoutSessionsController.handleCreateCheckoutSession,
  }
]);

app.use(router.middleware()).use(
  KoaCors({
    origin: process.env.CORS_ALLOW_ORIGINS,
  })
);

const port: number = parseInt(process.env.SERVER_PORT as string) || 3030;
app.listen(port, () => {
  console.log("API server listening on port ", port);
});
